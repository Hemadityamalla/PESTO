#! env python3

import sys
import os
from shutil import copy2

import numpy as np
import scipy.constants as co

from pesto import Domain, Solution, Controller, Photoionization, NTFF
from pesto.param import (param, ParameterContainer, ParameterNotAllowed,
                         contained_in)

def main():
    ###
    ### ARRANGE INPUT / OUTPUT
    ###
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("input",
                        help="The input file (.yaml/.json/.h5)")

    args = parser.parse_args()

    params = Parameters()

    params.file_load(args.input)
    params.report()

    rid = os.path.splitext(args.input)[0]

    out_dir = os.path.join(os.path.split(os.path.abspath(args.input))[0], rid)
    try:
        os.mkdir(out_dir)
    except OSError:
        pass

    copy2(args.input, out_dir)
    params.json_dumpf(os.path.join(out_dir, 'parameters.json'))

    ###
    ### SET SIMULATION PARAMETERS
    ###
    
    n, m, l = params.n, params.m, params.l
    Lz = params.Lz
    Lr = params.Lr if params.Lr > 0 else Lz * n / m
    
    if (n + 2 * l + 1) % 32 != 0:
        raise ValueError("(n + 2 * l + 1) must be multiple of 32 ")

    if abs(m * Lr / (n * Lz) - 1) > 1e-5:
        print("(m * Lr / (n * Lz)) = %g" % abs(m * Lr / (n * Lz)))
        raise ValueError("At the moment, only allowing dr = dz")

    
    dom = Domain(n, m, l, Lr, Lz)
    dom.pretty_print()
    
    # Initialize electron density
    ne = dom.zeros_host()
    l = params.l
    w = params.seed_width
    z0 = params.seed_z0
    ne[l:-l - 1, l:-l - 1] += (params.seed_max_dens *
                               np.exp(-(dom.rc[:, None]**2 / w**2)
                                      -(dom.zc[None, :] - z0)**2 / w**2))

    ne[l:-l - 1, l:-l - 1] += params.background_ionization
    
    t0, t1, dt = params.time_range
    out_times = np.r_[t0:t1:dt]

    ###
    ### RUN THE SIMULATION
    ###
    dtype = np.dtype(params.dtype).type
    sol = Solution(dom, dtype=dtype)
    sol.setall(ne=ne, ez=params.background_electric_field)
    sol.to_gpu()

    photo = None
    if params.photoionization:
        photo = Photoionization(dom, pO2=params.oxygen_partial_p,
                                dtype=dtype)
        photo.to_gpu()
    
    cont = Controller(dom, sol, photo=photo)
    
    cont.init_output(out_dir)
    
    dom.data_reduction = params.data_reduction
    sol.data_reduction = params.data_reduction
    
    cont.run(out_times)

    
class Parameters(ParameterContainer):
    @param(default="")
    def family(s):
        """ A descriptor for a family of runs. """
        return str(s)
    
    @param()
    def n(s):
        """ Number of grids cells in the r dimension. """
        return int(s)

    @param()
    def m(s):
        """ Number of grids cells in the r dimension. """
        return int(s)

    @param()
    def l(s):
        """ Number of CPML cells. """
        return int(s)
        
    @param()
    def Lz(s):
        """ Domain extension in the z dimension. """
        return float(s)

    @param()
    def Lr(s):
        """ Domain extension in the r dimension. """
        return float(s)
    
    @param(default=1e21)
    def seed_max_dens(s):
        """ Peak electron density in the initial seed. """
        return float(s)

    @param(default=.2 * co.milli)
    def seed_width(s):
        """ Width of the initial seed. """
        return float(s)

    @param(default=.2 * co.milli)
    def seed_z0(s):
        """ Location of the seed. """
        return float(s)
        
    @param()
    def background_ionization(s):
        """ Background electron density. """
        return float(s)

    @param()
    def background_electric_field(s):
        """ External electric field. """
        return float(s)

    @param(default=False)
    def photoionization(s):
        """ Is photo-ionization included? """
        return s

    @param(default=150 * co.torr)
    def oxygen_partial_p(s):
        """ Oxygen partial pressure """
        return s

    @param(default=0.0372193)
    def e_mobility(s):
        """ Electron mobility. """
        return float(s)

    @param(default=co.elementary_charge)
    def e_charge(s):
        """ Electron charge in C. """
        return float(s)

    @param(default=0.18)
    def e_diffusion(s):
        """ Electron diffusion coefficient. """
        return float(s)

    @param(default=433200.0)
    def ionization_alpha(s):
        """ alpha in the parametrization of ionization """
        return float(s)

    @param(default=2e7)
    def ionization_field(s):
        """ field in the parametrization of ionization """
        return float(s)

    @param(default=2000.0)
    def attachment_alpha(s):
        """ alpha in the parametrization of dissociative attachment """
        return float(s)

    @param(default=3e6)
    def attachment_field(s):
        """ field in the parametrization of dissociative attachment """
        return float(s)

    @param(contained_in({'float64', 'float32'}), default='float64')
    def dtype(s):
        """ The precision type (float32/float64). """
        return s

    @param(default=1)
    def data_reduction(s):
        """ Reduce data output this factor in each dimension. """
        v = int(s)
        if 32 % v != 0:
            raise ParameterNotAllowed("data_reduction must divide 32.")

        return v

    @param()
    def time_range(s):
        """ Time range of the full simulation. """
        v = [float(x) for x in s.split(':')]

        if len(v) != 3:
            raise ParameterNotAllowed("Use start:end:step for time ranges")

        return v


if __name__ == '__main__':
    main()


