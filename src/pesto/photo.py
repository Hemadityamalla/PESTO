from collections import namedtuple

import numpy as np
import scipy.constants as co

GPUVars = namedtuple("GPUVars",
                     ["alpha", "C", "phi0", "phi1r", "phi1z"])


class Photoionization():
    QUENCHING_PRESSURE = 30 * co.torr
    
    def __init__(self, dom, ptotal=1 * co.bar, pO2=150 * co.torr,
                 dtype=np.float64):
        self.dom = dom

        # Celestin's table III.3 in his thesis
        self.A    =  (np.array([0.0067, 0.0346, 0.3059])
                      * co.centi**-1 * co.torr**-1)
        self.lmbd =  (np.array([0.0447,  0.1121, 0.5994])
                      * co.centi**-1 * co.torr**-1)

        self.efficiency = 0.06
        self.ptotal = ptotal
        
        self.pO2 = pO2

        self.ng    = len(self.A)
        self.gpuvars = None
        self.dtype = dtype

        self.init()

        
    def init(self):
        """ Calculate C and alpha for the given parameters.  Call this if
        you change the default values. """
        # We include all prefactors into the multiplications constants
        # for each group, namely photo_efficiency and quenching

        self.quenching = (self.QUENCHING_PRESSURE /
                          (self.ptotal + self.QUENCHING_PRESSURE))

        self.C     = (self.A * self.pO2 * co.c
                      * self.efficiency * self.quenching)

        self.alpha = self.lmbd * self.pO2 * co.c
        
        
    def to_gpu(self):
        from .cuda import gpu

        ng = self.ng
        nx = self.dom.nx
        mx = self.dom.mx
        dtype = self.dtype
        
        self.gpuvars = GPUVars(gpu.to_gpu(self.alpha.astype(dtype)),
                               gpu.to_gpu(self.C.astype(dtype)),
                               gpu.zeros((ng, nx, mx), dtype),
                               gpu.zeros((ng, nx, mx), dtype),
                               gpu.zeros((ng, nx, mx), dtype))
        
                               

        
        
