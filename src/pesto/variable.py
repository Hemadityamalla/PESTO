import numpy as np


class Variable(object):
    INDEX = {}
    def __init__(self, func, doc=None, name="", units="?"):
        self.func = func
        self.name = name
        self.units = units
        self.doc = doc

    def __call__(self, *args):
        r = self.func(*args)
        return r
        
    @staticmethod
    def add(**kwargs):
        """ A decorator to define variables from functions. """
        def deco(f):
            kwargs.update({'doc': f.__doc__})
            Variable.INDEX[f.__name__] = Variable(f, **kwargs)
            return f

        return deco

variable = Variable.add

@Variable.add(name="Radial electric field, $E_r$", units="V/m")
def er(sol):
    n, m, l = sol.dom.n, sol.dom.m, sol.dom.l
    return sol.dom.rf, sol.dom.zc, sol.er[l: l + n + 1, l: l + m]


@Variable.add(name="Longitudinal electric field, $E_z$", units="V/m")
def ez(sol):
    n, m, l = sol.dom.n, sol.dom.m, sol.dom.l
    return sol.dom.rc, sol.dom.zf, sol.ez[l: l + n, l: l + m + 1]

@Variable.add(name="Azimuthal magnetic field, $H_\phi$", units="")
def hphi(sol):
    n, m, l = sol.dom.n, sol.dom.m, sol.dom.l
    return sol.dom.rf, sol.dom.zf, sol.hphi[l: l + n + 1, l: l + m + 1]

@Variable.add(name="Radial current, $j_r$", units="A/m^2")
def jr(sol):
    n, m, l = sol.dom.n, sol.dom.m, sol.dom.l
    return sol.dom.rf, sol.dom.zc, sol.jr[l: l + n + 1, l: l + m]


@Variable.add(name="Longitudinal current, $j_z$", units="A/m^2")
def jz(sol):
    n, m, l = sol.dom.n, sol.dom.m, sol.dom.l
    return sol.dom.rc, sol.dom.zf, sol.jz[l: l + n, l: l + m + 1]


@Variable.add(name="Absolute value of the electric field, $|E|$", units="V/m")
def eabs(sol):
    n, m, l = sol.dom.n, sol.dom.m, sol.dom.l
    er_avg = 0.5 * (sol.er[l: l + n, l: l + m] +
                    sol.er[l + 1: l + n + 1, l: l + m])
    ez_avg = 0.5 * (sol.ez[l: l + n, l: l + m] +
                    sol.ez[l: l + n, l + 1: l + m + 1])
    eabs = np.sqrt(er_avg**2 + ez_avg**2)
    
    return sol.dom.rc, sol.dom.zc, eabs


@Variable.add(name="Electron density, $n_e$", units="m^{-3}")
def ne(sol):
    n, m, l = sol.dom.n, sol.dom.m, sol.dom.l
    return sol.dom.rc, sol.dom.zc, sol.ne[l: l + n, l: l + m]

