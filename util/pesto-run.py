#!/usr/bin/env python3

import sys
from importlib import import_module

appname = sys.argv[1]
sys.argv = sys.argv[1:]
            
m = import_module("pesto.apps." + appname)
m.main()
